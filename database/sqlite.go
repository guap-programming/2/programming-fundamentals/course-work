package database

import (
	"github.com/jinzhu/gorm"
)

// GetSqliteDb returns instance of SqliteDb
func GetSqliteDb() *gorm.DB {
	db, err := gorm.Open("sqlite3", "test.db")
	if err != nil {
		panic("failed to connect database:" + " " + err.Error())
	}

	return db
}
