package usecase

import (
	"context"
	"github.com/jinzhu/gorm"
	"gitlab.com/guap-programming/2/programming-fundamentals/course-work/internal/note"
	"gitlab.com/guap-programming/2/programming-fundamentals/course-work/internal/note/models"
	"strconv"
)

type Usecase struct {
	r note.Repository
}

func NewUsecase(r note.Repository) *Usecase {
	return &Usecase{r: r}
}

func (u Usecase) Create(ctx context.Context, ln string, n string, bd [3]int) error {
	return u.r.Create(ctx, &models.Note{
		Lastname:   ln,
		Name:       n,
		BirthDay:   bd[0],
		BirthMonth: bd[1],
		BirthYear:  bd[2],
	})
}

func (u Usecase) FindByMonth(ctx context.Context, month int) (*models.List, error) {
	notes, err := u.r.FindByMonth(ctx, month)
	if err != nil {
		return nil, err
	}

	list := models.New()
	for _, n := range notes {
		list.PushFront(n)
	}

	return list, nil
}

func (u Usecase) Update(ctx context.Context, id string, ln string, n string, bd [3]int) error {
	intId, err := strconv.Atoi(id)
	if err != nil {
		return err
	}

	return u.r.Update(ctx, id, &models.Note{
		Model:      gorm.Model{ID: uint(intId)},
		Lastname:   ln,
		Name:       n,
		BirthDay:   bd[0],
		BirthMonth: bd[1],
		BirthYear:  bd[2],
	})
}

func (u Usecase) Sort(_ context.Context, list *models.List, less func(i *models.Element, j *models.Element) bool) (*models.List, error) {
	list.Sort(less)
	return list, nil
}

func (u Usecase) Remove(ctx context.Context, id string) error {
	return u.r.Remove(ctx, id)
}
