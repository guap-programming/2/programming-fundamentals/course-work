package models

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_insertionSort(t *testing.T) {
	l := New()
	l.PushBack(1)
	l.PushBack(2)
	l.PushBack(3)
	l.PushBack(4)
	l.PushBack(5)

	first := l.root.next
	second := first.next
	third := second.next
	fourth := third.next
	fifth := fourth.next
	assert.Equal(t, 1, first.Value)
	assert.Equal(t, 2, second.Value)
	assert.Equal(t, 3, third.Value)
	assert.Equal(t, 4, fourth.Value)
	assert.Equal(t, 5, fifth.Value)

	l.Sort(func(i *Element, j *Element) bool {
		return i.Value.(int) > j.Value.(int)
	})

	first = l.root.next
	second = first.next
	third = second.next
	fourth = third.next
	fifth = fourth.next

	assert.Equal(t, 5, first.Value)
	assert.Equal(t, 4, second.Value)
	assert.Equal(t, 3, third.Value)
	assert.Equal(t, 2, fourth.Value)
	assert.Equal(t, 1, fifth.Value)
}
