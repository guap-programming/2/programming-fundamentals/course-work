package models

import "github.com/jinzhu/gorm"

// Note is a structure of person note
type Note struct {
	gorm.Model
	Lastname string
	Name     string
	BirthDay   int
	BirthMonth int
	BirthYear  int
}
