package note

import (
	"context"
	"gitlab.com/guap-programming/2/programming-fundamentals/course-work/internal/note/models"
)

// Usecase is a main interface of application
type Usecase interface {
	Create(ctx context.Context, ln string, n string, bd [3]int) error
	FindByMonth(ctx context.Context, month int) (*models.List, error)
	Update(ctx context.Context, id string, ln string, n string, bd [3]int) error
	Sort(ctx context.Context, list *models.List, less func(i *models.Element, j *models.Element) bool) (*models.List, error)
	Remove(ctx context.Context, id string) error
}
