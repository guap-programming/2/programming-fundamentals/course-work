package grpc

import (
	"context"
	"gitlab.com/guap-programming/2/programming-fundamentals/course-work/internal/note"
	apipb "gitlab.com/guap-programming/2/programming-fundamentals/course-work/internal/note/delivery/grpc/api"
	"gitlab.com/guap-programming/2/programming-fundamentals/course-work/internal/note/models"
)

type Server struct {
	u note.Usecase
}

func NewServer(u note.Usecase) *Server {
	return &Server{u: u}
}

func (s Server) Create(ctx context.Context, req *apipb.CreateRequest) (*apipb.CreateResponse, error) {
	err := s.u.Create(ctx, req.Note.LastName, req.Note.Name, [3]int{int(req.Note.BirthDay), int(req.Note.BirthMonth), int(req.Note.BirthYear)})

	return &apipb.CreateResponse{Success: err == nil}, nil
}

func (s Server) FindByMonth(ctx context.Context, req *apipb.FindByMonthRequest) (*apipb.FindByMonthResponse, error) {
	notesList, err := s.u.FindByMonth(ctx, int(req.Month))
	if err != nil {
		return nil, err
	}

	notes := make([]*apipb.Note, 0, notesList.Len())
	for i := notesList.Front(); i != nil ; i = i.Next()  {
		n := &apipb.Note{
			LastName:   i.Value.(*models.Note).Lastname,
			Name:       i.Value.(*models.Note).Name,
			BirthDay:   int32(i.Value.(*models.Note).BirthDay),
			BirthMonth: int32(i.Value.(*models.Note).BirthMonth),
			BirthYear:  int32(i.Value.(*models.Note).BirthYear),
		}
		notes = append(notes, n)
	}
	return &apipb.FindByMonthResponse{
		Notes:                notes,
	}, nil
}

func (s Server) Update(ctx context.Context, req *apipb.UpdateRequest) (*apipb.UpdateResponse, error) {
	err := s.u.Update(ctx, req.Id, req.Note.LastName, req.Note.Name, [3]int{int(req.Note.BirthDay), int(req.Note.BirthMonth), int(req.Note.BirthYear)})

	return &apipb.UpdateResponse{Success: err == nil}, nil
}

func (s Server) Remove(ctx context.Context, req *apipb.RemoveRequest) (*apipb.RemoveResponse, error) {
	err := s.u.Remove(ctx, req.Id)

	return &apipb.RemoveResponse{Success: err == nil}, nil
}
