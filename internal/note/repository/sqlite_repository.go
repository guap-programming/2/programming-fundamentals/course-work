package repository

import (
	"context"
	"github.com/jinzhu/gorm"
	"gitlab.com/guap-programming/2/programming-fundamentals/course-work/internal/note/models"
	"strconv"
)

//SqliteNoteRepository implement repository interface
type SqliteNoteRepository struct {
	db *gorm.DB
}

//NewSqliteNoteRepository constructor for SqliteNoteRepository
func NewSqliteNoteRepository(db *gorm.DB) *SqliteNoteRepository {
	db.AutoMigrate(&models.Note{})
	return &SqliteNoteRepository{db: db}
}

//Create note
func (s SqliteNoteRepository) Create(_ context.Context, note *models.Note) error {
	return s.db.Save(note).Error
}

//FindByMonth returns list
func (s SqliteNoteRepository) FindByMonth(_ context.Context, month int) ([]*models.Note, error) {
	notes := make([]*models.Note, 0) // can be optimized by 3rd param
	err := s.db.Where(&models.Note{BirthMonth: month}).Find(&notes).Error

	return notes, err
}

//Update note
func (s SqliteNoteRepository) Update(_ context.Context, id string, note *models.Note) error {
	intID, err := strconv.Atoi(id)
	if err != nil {
		return err
	}

	dbNote := models.Note{}
	s.db.First(&dbNote, intID)
	note.ID = dbNote.ID
	note.CreatedAt = dbNote.CreatedAt
	note.DeletedAt = dbNote.DeletedAt
	note.UpdatedAt = dbNote.UpdatedAt

	return s.db.Model(dbNote).Update(note).Error
}

//Remove note
func (s SqliteNoteRepository) Remove(_ context.Context, id string) error {
	intID, err := strconv.Atoi(id)
	if err != nil {
		return err
	}

	note := models.Note{}
	err = s.db.First(&note, intID).Error
	if err != nil {
		return err
	}

	return s.db.Delete(&note).Error
}
