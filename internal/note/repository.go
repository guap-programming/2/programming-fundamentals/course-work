package note

import (
	"context"
	"gitlab.com/guap-programming/2/programming-fundamentals/course-work/internal/note/models"
)

// Repository is a main repository interface for note
type Repository interface {
	Create(ctx context.Context, note *models.Note) error
	FindByMonth(ctx context.Context, month int) ([]*models.Note, error)
	Update(ctx context.Context, id string, note *models.Note) error
	Remove(ctx context.Context, id string) error
}
