.PHONY: gen-proto

gen-proto:
	 protoc -I. api/note.proto --go_out=plugins=grpc:internal/note/delivery/grpc
