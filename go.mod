module gitlab.com/guap-programming/2/programming-fundamentals/course-work

go 1.13

require (
	github.com/golang/protobuf v1.3.2
	github.com/jinzhu/gorm v1.9.11
	github.com/kr/pretty v0.2.0 // indirect
	github.com/stretchr/testify v1.4.0
	google.golang.org/grpc v1.19.0
)
