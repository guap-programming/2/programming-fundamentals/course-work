package main

import (
	"fmt"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gitlab.com/guap-programming/2/programming-fundamentals/course-work/database"
	grpcInstance "gitlab.com/guap-programming/2/programming-fundamentals/course-work/internal/note/delivery/grpc"
	apipb "gitlab.com/guap-programming/2/programming-fundamentals/course-work/internal/note/delivery/grpc/api"
	"gitlab.com/guap-programming/2/programming-fundamentals/course-work/internal/note/repository"
	"gitlab.com/guap-programming/2/programming-fundamentals/course-work/internal/note/usecase"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
)

func main() {
	lis, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatal(fmt.Sprintf("failed to listen %v", err))
	}
	log.Print("server has started at " + ":50051")

	grpcServer := grpc.NewServer()
	reflection.Register(grpcServer)

	db := database.GetSqliteDb()
	defer db.Close()

	r := repository.NewSqliteNoteRepository(db)
	u := usecase.NewUsecase(r)

	apipb.RegisterNoteServiceServer(grpcServer, grpcInstance.NewServer(u))
	err = grpcServer.Serve(lis)

	if err != nil {
		log.Fatal(err.Error())
	}
	//db := database.GetSqliteDb()
	//defer db.Close()
	//
	//r := repository.NewSqliteNoteRepository(db)
	//u := usecase.NewUsecase(r)
	//ctx := context.Background()
	//
	//err := u.Create(ctx, "test", "test", [3]int{01, 10, 1970})
	//if err != nil {
	//	log.Fatal("create error: ", err)
	//}
	//
	//list, err := u.FindByMonth(ctx, 10)
	//if err != nil {
	//	log.Fatal("find by month error: ", err)
	//}
	//
	//sortedList, err := u.Sort(ctx, list, func(i *models.Element, j *models.Element) bool {
	//	return i.Value.(*models.Note).ID < j.Value.(*models.Note).ID
	//})
	//_ = sortedList
	//
	//if err != nil {
	//	log.Fatal("sort error: ", err)
	//}
	//
	//lastInsertedId := strconv.Itoa(int(list.Back().Value.(*models.Note).ID))
	//err = u.Update(ctx, lastInsertedId, "updated", "", [3]int{})
	//if err != nil {
	//	log.Fatal("update error: ", err)
	//}
}
